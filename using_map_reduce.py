import sys
from fun import foo
from time import time


N = sys.argv[1]


table = [1]*int(N)
t=time()
print reduce(lambda x,y:x and y, map(foo, table), True) 
print time()-t
