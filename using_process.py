import sys
from fun import foo
from time import time
from multiprocessing import Pool
N = sys.argv[1]


table = [1]*int(N)

t=time()
pool = Pool(8)
print all(pool.map(foo, table))
print time()-t
