import sys
from fun import foo
from time import time
N = sys.argv[1]


table = [1]*int(N)
t=time()
for s in table:
    if not foo(s):
        print False
print True
print time()-t
