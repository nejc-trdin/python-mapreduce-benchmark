## Benchmark simple single machine Python MapReduce

A list of `N` elements is constructed, each element is a `1`. All elements are mapped using function `fun.foo()`, but the function first counts to 10 and adds the values to a dummy variable. Then all elements of a new list are checked, if the equal `True`.

There are 5 implementations:
1. Unrolled loop (`using_naive.py`). Looping through all list elements, mapping each element and checking if it is `True`.
2. Using function `all()` (`using_all.py`). A new list is constructed, mapping all elements to `True`/`False`. `all()` checks if all list elements are `True`.
3. Using built-in `map()` and `reduce()` (`using_map_reduce.py`). All elements are mapped using `map()` and `reduce()` works with a `lambda` function.
4. Using threads (`using_thread.py`). A pool of 10 threads is constructed for mapping elements. Then `all()` is used for checking elements.
5. Using processes (`using_process.py`). A pool of 10 processes is constructed for mapping elements. Then `all()` is used for checking elements.

Each run outputs `True` (if all mapped elements are `True`) or `False` (if any element is `False`).

### Execution

```sh
$ python using_naive.py 100000000
$ python using_all.py 100000000
$ python using_map_reduce.py 100000000
$ python using_thread.py 100000000
$ python using_process.py 100000000
```

### Results

```sh
$ time python using_naive.py 100000000
True
45.6260488033

real    0m46.395s
user    0m46.181s
sys     0m0.136s
$ time python using_all.py 100000000
True
42.1942448616

real    0m42.965s
user    0m42.807s
sys     0m0.096s
$ time python using_map_reduce.py 100000000
True
44.8342449665

real    0m45.583s
user    0m45.166s
sys     0m0.352s
$ time python using_thread.py 100000000
True
76.030520916

real    1m16.951s
user    1m26.797s
sys     0m30.028s
$ time python using_process.py 100000000
True
13.5652370453

real    0m14.483s
user    1m26.518s
sys     0m2.210s
```
