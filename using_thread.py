import sys
from fun import foo
from time import time
from multiprocessing.pool import ThreadPool
N = sys.argv[1]


table = [1]*int(N)

t=time()
pool = ThreadPool(10)
print all(pool.map(foo, table))
print time()-t
